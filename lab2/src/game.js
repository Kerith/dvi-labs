var sprites = {
  // Sprite: {Props}
  frog: { sx: 0, sy: 0, w: 48, h: 48, frames: 1 },
  bg: { sx: 433, sy: 0, w: 320, h: 480, frames: 1 },
  car1: { sx: 143, sy: 0, w: 48, h: 48, frames: 1 },
  car2: { sx: 191, sy: 0, w: 48, h: 48, frames: 1 },  
  car3: { sx: 239, sy: 0, w: 96, h: 48, frames: 1 },
  car4: { sx: 335, sy: 0, w: 48, h: 48, frames: 1 },
  car5: { sx: 383, sy: 0, w: 48, h: 48, frames: 1 },
  log: { sx: 287, sy: 383, w: 144, h: 48, frames: 1 },
  death: { sx: 0, sy: 143, w: 48, h: 48, frames: 4 },
  water: { sx: 433, sy: 48, w: 320, h: 114, frames: 1 },
  home: { sx: 433, sy: 0, w: 320, h: 48, frames: 1 },
  spawner: { sx: 0, sy: 0, w: 0, h: 0, frames: 1 }
};

var CELL_WIDTH = 48;

var initCarYs = function(num) {
  var ret = new Array(num);
  var gh = Game.height;
  var cw = CELL_WIDTH;
  for (var i = 0; i < num; i++) {
    ret[i] = gh - cw;
    cw += CELL_WIDTH;
  }

  return ret;
};

var initLogYs = function(num) {
  var ret = new Array(num);
  var cw = CELL_WIDTH;
  for (var i = 0; i < num; i++) {
    ret[i] = cw;
    cw += CELL_WIDTH;
  }

  return ret;
};

var cars = {
  c1: {sprite: 'car1', props: {vx: -40, n: 0, z: 2}}, 
  c2: {sprite: 'car2', props: {vx: 30, n: 1, z: 2}}, 
  c3: {sprite: 'car3', props: {vx: -50, n: 2, z: 2}}, 
  c4: {sprite: 'car4', props: {vx: -20, n: 3, z: 2}}, 
  c5: {sprite: 'car5', props: {vx: 30, n: 4, z: 2}} 
};

var OBJ_BACKGROUND = 1,
    OBJ_CAR = 2,
    OBJ_LOG = 4,
    OBJ_WATER = 8,
    OBJ_FROG = 16,
    OBJ_DEATH = 32,
    OBJ_SPAWNER = 64;

var car_xs;
var car_ys;
var log_xs;
var log_ys;


var startGame = function() {

  car_xs = [Game.width, -CELL_WIDTH, Game.width, Game.width, -CELL_WIDTH];
  car_ys = initCarYs(5);
  log_xs = [-CELL_WIDTH, Game.width, -CELL_WIDTH, 0];
  log_ys = initLogYs(4);

  var bg = new GameBoard();
  bg.add(new Background());
  Game.setBoard(1,bg);


  Game.setBoard(2,respawnGameObjects());

  Game.setBoard(3,new TitleScreen("Frog Souls", 
                                  "Prepare to jump",
                                  playGame));

  Game.setBoard(4, new TitleScreen('You Died...', 
                                   '', 
                                   playGame));

  Game.setBoard(5, new TitleScreen('You defeated...', 
                                   'You got 300000 souls', 
                                   playGame));

  Game.deactivateBoards();

  Game.setActiveBoard(3);
};

var playGame = function() {
  Game.deactivateBoards();

  Game.setBoard(2, respawnGameObjects(gameBoard));

  Game.setActiveBoard(1);
  Game.setActiveBoard(2);
};

var loseGame = function () {
  Game.deactivateBoards();
  
  Game.setActiveBoard(4);
};

var winGame = function() {
  Game.deactivateBoards();

  Game.setActiveBoard(5);
};

var respawnGameObjects = function() {
  gameBoard = new GameBoard();
  gameBoard.add(new Spawner(new Car(cars.c2), {minWait: 4}));
  gameBoard.add(new Spawner(new Car(cars.c3), {minWait: 6}));
  gameBoard.add(new Spawner(new Car(cars.c4), {minWait: 7}));
  gameBoard.add(new Spawner(new Car(cars.c5), {minWait: 5}));
  gameBoard.add(new Spawner(new Log({n: 0, vx: 35, z: 9}), {minWait: 6}));
  gameBoard.add(new Spawner(new Log({n: 1, vx: -50, z: 9}), {minWait: 13}));
  gameBoard.add(new Spawner(new Log({n: 2, vx: 60, z: 9}), {minWait: 8}));
  gameBoard.add(new Water());
  gameBoard.add(new Home());
  gameBoard.add(new Frog());

  return gameBoard;

};

var Background = function() {
  this.setup('bg', {z: 10});

  this.x = 0;
  this.y = 0;
  this.step = function() {

  };
};

Background.prototype = new Sprite();
Background.prototype.type = OBJ_BACKGROUND;

var Frog = function() {
  this.setup('frog', {waitTime: 0.5, vx: 0, z: 1000});

  this.x = (Game.width - CELL_WIDTH) / 2;
  this.y = Game.height - CELL_WIDTH;

  this.wait = this.waitTime;
  this.movable = true;

  this.isOnLog = false;
};

Frog.prototype = new Sprite();
Frog.prototype.type = OBJ_FROG;

Frog.prototype.step = function(dt) {

  // Update movable
  this.wait -= dt;
  if (this.wait <= 0) {
    this.movable = true;
    this.wait = this.waitTime;
  }

  // Handle input
  if (this.movable) {
    if (Game.keys['up'] && this.y > 0) {
      this.y -= CELL_WIDTH;
      this.movable = false;
    } 
    if (Game.keys['down'] && this.y < Game.height) {
      this.y += CELL_WIDTH;
      this.movable = false;
    }
    if (Game.keys['left'] && this.x >= 0) {
      this.x -= CELL_WIDTH;
      this.movable = false;
    }
    if (Game.keys['right'] && this.x < Game.width) {
      this.x += CELL_WIDTH;
      this.movable = false;
    }
  }

  this.x += this.vx * dt;
  this.vx = 0;
  this.isOnLog = false;

};

Frog.prototype.hit = function(damage) {
  if (!this.isOnLog) {
    if(this.board.remove(this)) {
      var props = {};
      props.x = this.x;
      props.y = this.y;
      this.board.add(new Death(props));
    }
  }
};

Frog.prototype.onLog = function(vLog) {
  this.vx = vLog;
  this.isOnLog = true;
};

Frog.prototype.onHome = function() {
  winGame();
};

var Car = function (base) {
  this.setup(base.sprite,base.properties);
  this.merge(base.props);
  this.x = car_xs[this.n];
  this.y = car_ys[this.n];

  this.minWait = 5;
  this.wait = this.minWait;
  this.spawner = true;
};

Car.prototype = new Sprite();
Car.prototype.type = OBJ_CAR;

Car.prototype.step = function (dt) {
  
  var collision = this.board.collide(this,OBJ_FROG);
  if(collision) {
    collision.hit(this.damage);
  } 

  // Update position
  if ((this.vx > 0 && this.x >= Game.width) ||
      (this.vx < 0 && this.x <= 0)) {
    this.board.remove(this);
  } else  {
    this.x += this.vx * dt;
  }
  
};

Car.prototype.spawnCar = function() {
  var i = Math.floor(Math.random() * 5) + 1;
  switch(i) {
    case 1:
      this.board.add(new Car(cars.c1));
    case 2:
      this.board.add(new Car(cars.c2));
    case 3:
      this.board.add(new Car(cars.c5));
    case 4:
      this.board.add(new Car(cars.c4));
    case 5:
      this.board.add(new Car(cars.c5)); 
  };

  this.spawner = false;
  console.log('new car created[' + i + ']');
  
};

var Log = function(props) {
  this.setup('log', props);

  this.x = log_xs[this.n];
  this.y = log_ys[this.n];

};

Log.prototype = new Sprite();
Log.prototype.type = OBJ_LOG;

Log.prototype.step = function(dt) {

  var collision = this.board.collide(this,OBJ_FROG);
  if(collision) {
    collision.onLog(this.vx);
  } 

  // Update position
  if ((this.vx > 0 && this.x >= Game.width) ||
      (this.vx < 0 && this.x <= 0)) {
    this.board.remove(this);
  } else  {
    this.x += this.vx * dt;
  }
};

var Water = function () {
  this.setup('water', {});
  this.x = 0;
  this.y = CELL_WIDTH;
  this.z = 10;
};

Water.prototype = new Sprite();
Water.prototype.type = OBJ_WATER;

Water.prototype.step = function(dt) {
  var collision = this.board.collide(this,OBJ_FROG);
  if(collision) {    
    collision.hit(this.vx);
  } 
};

Water.prototype.draw = function(ctx) {
  return;
};

var Death = function(props) {
  this.setup('death', props);
};

Death.prototype = new Sprite();
Death.prototype.type = OBJ_DEATH;

Death.prototype.step = function(dt) {
  this.frame++;
  if(this.frame >= 8) {
    loseGame();
    this.board.remove(this);
  }
};

var Home = function () {
  this.setup('home', {});
  this.x = 0;
  this.y = 0;
};

Home.prototype = new Sprite();
Home.prototype.type = OBJ_WATER;

Home.prototype.step = function(dt) {
  var collision = this.board.collide(this,OBJ_FROG);
  if(collision) {    
    collision.onHome();
  } 
};

Home.prototype.draw = function(ctx) {
  return;
};

var Spawner = function(obj, props) {
  this.setup('spawner', props);

  this.obj = obj;

  this.x = 0;
  this.y = 0; 

  this.wait = 0;
};

Spawner.prototype = new Sprite();
Spawner.prototype.type = OBJ_SPAWNER;

Spawner.prototype.step = function(dt) {

  // Update wait, spawn if necessary
  this.wait -= dt;
  if (this.wait <= 0) {    
    this.wait = this.minWait + Math.random();
    //Spawn object    
    var n = Object.create(this.obj);    
    this.board.add(n);
  } 

};


window.addEventListener("load", function() {
  Game.initialize("game",sprites,startGame);
});


