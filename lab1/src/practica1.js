/**
 * MemoryGame es la clase que representa nuestro juego. Contiene un array con la cartas del juego,
 * el número de cartas encontradas (para saber cuándo hemos terminado el juego) y un texto con el mensaje
 * que indica en qué estado se encuentra el juego
 */
var MemoryGame = MemoryGame || {};

var CardStates = {HIDDEN: "Hidden", SHOWING: "Showing"};

/**
 * Constructora de MemoryGame
 */
MemoryGame = function(gs) {
	this.gs = gs;
	this.header = "Hello World";
	this.cards = [new MemoryGame.Card("potato"),
				  new MemoryGame.Card("potato"),
				  new MemoryGame.Card("8-ball"),
				  new MemoryGame.Card("8-ball"),
				  new MemoryGame.Card("dinosaur"),
				  new MemoryGame.Card("dinosaur"),
				  new MemoryGame.Card("kronos"),
				  new MemoryGame.Card("kronos"),
				  new MemoryGame.Card("rocket"),
				  new MemoryGame.Card("rocket"),
				  new MemoryGame.Card("unicorn"),
				  new MemoryGame.Card("unicorn"),
				  new MemoryGame.Card("guy"),
				  new MemoryGame.Card("guy"),
				  new MemoryGame.Card("zeppelin"),
				  new MemoryGame.Card("zeppelin"),
				];
	this.prevCard = -1;
	this.clickable = true;
};


MemoryGame.prototype.initGame = function() {	
	this.draw();
	this.shuffle();
	this.loop();
};

MemoryGame.prototype.draw = function() {
	var length = this.cards.length;
	var card = null;
	for (i = 0;i < length;i++) {
		this.cards[i].draw(this.gs, i);		
	}

};

MemoryGame.prototype.loop = function() {
	var self = this;
	setInterval(function(){self.draw()}, 16);
};

MemoryGame.prototype.shuffle = function() {
	var currentIndex = this.cards.length, temporaryValue, randomIndex;

   	// While there remain elements to shuffle...
	while (0 !== currentIndex) {  // Pick a remaining element...
		randomIndex = Math.floor(Math.random() * currentIndex);
		currentIndex -= 1;  // And swap it with the current element.
		temporaryValue = this.cards[currentIndex];
		this.cards[currentIndex] = this.cards[randomIndex];
		this.cards[randomIndex] = temporaryValue;
	}

};

MemoryGame.prototype.onClick = function(cardId) {
	var card = this.cards[cardId];
	var self = this;
	if (card.isFlippable() && this.clickable && this.prevCard !== cardId) {

		card.flip();

		if (this.prevCard !== -1) {

			var other = this.cards[this.prevCard];
			if (card.compareTo(other)) {
				//Found a match
				this.gs.drawMessage("Good!");
				card.found();
				other.found();

				if (this.checkFinished()) {
					this.gs.drawMessage("You're ready for Dark Souls.");
				}			
			} else {
				//Wrong card
				this.clickable = false;
				this.gs.drawMessage("Wrong!");
				setTimeout(function() {card.flip();
									   other.flip();
									   self.clickable = true;
									   self.gs.drawMessage("C'mon, try again.")}, 
							1000);
			}
			this.prevCard = -1;
		} else {
			//No other card selected
			this.prevCard = cardId;
		}
	}
};

MemoryGame.prototype.checkFinished = function(){
	var res = true;
	var length = this.cards.length;
	for (i = 0;i < length;i++) {
		if(this.cards[i].isFlippable()) {
			res = false;
		}	
	}

	return res;
};

 /**
 * Constructora de las cartas del juego. Recibe como parámetro el nombre del sprite que representa la carta.
 * Dos cartas serán iguales si tienen el mismo sprite.
 * La carta puede guardar la posición que ocupa dentro del tablero para luego poder dibujarse
 * @param {string} id Nombre del sprite que representa la carta
 */
MemoryGame.Card = function(id) {
	this.state = CardStates.HIDDEN;	
	this.sprite = id;	
	this.flippable = true;
	console.log("Card created: sprite[" + this.sprite +"]");
};

MemoryGame.Card.prototype.draw = function(gs, pos) {
	if (this.state === CardStates.SHOWING) {
		gs.draw(this.sprite, pos);
	} else {
		gs.draw("back", pos);
	}
};

MemoryGame.Card.prototype.flip = function() {
	if (this.flippable) {
		if (this.state === CardStates.HIDDEN) {
			this.state = CardStates.SHOWING;
		} else {
			this.state = CardStates.HIDDEN;
		}
	}
};

MemoryGame.Card.prototype.found = function() {
	this.flippable = false;
};

MemoryGame.Card.prototype.compareTo = function(otherCard) {
	return (otherCard.sprite === this.sprite);
};

MemoryGame.Card.prototype.isFlippable = function () {return this.flippable;};
