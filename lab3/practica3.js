

window.addEventListener("load",function() {
	var Q = Quintus()
		.include("Sprites, Scenes, Input, 2D, Touch, UI, TMX, Anim")
		.setup({width: 320, height: 480})
		.controls().touch();

	/* === Components === */

	Q.component( "defaultEnemy", {

		added: function() {
			
			this.entity.on("bump.top",function(collision) {
                if(collision.obj.isA("Mario")) {
                	this.p.sheet = this.dieSheet;
                	this.play('die');
                    collision.obj.p.vy = -300;
                }
            });

            this.entity.on("bump.left,bump.right,bump.bottom", function(collision) {
                if (collision.obj.isA("Mario")) {
                    collision.obj.trigger('enemyHit');
                } 
            });

            this.entity.on('destroy', function() {this.destroy();});
        }        

	});


	/* === Animations === */

	Q.animations('mario', {
	 	run: { frames: [0,1,2], rate: 1/5}, 
	 	idle: { frames: [0], rate: 1/5 },
	 	jump: { frames: [0], rate: 1/5 },
	 	die: { frames: [0], rate: 1, loop: false, trigger: 'destroy'}	 	
	});

	Q.animations('enemy', {
		idle: { frames: [0,1], rate: 1/2 },
	 	die: { frames: [0], rate: 1/5, loop: false, trigger: 'destroy'}	 	
	});

	Q.animations('coin', {
		idle: { frames: [1,2], rate: 1/2 },
		collect: { frames: [0,1,2], rate: 1/15}
	});

	/* === Game Objets === */

	Q.Sprite.extend( "Mario", {
	  init: function(p) {
	    this._super({
	    	sheet: "marioR",
	    	sprite: 'mario',
	    	speed: 200,
	    	jumpSpeed: -500,
            x: p.x,
            y: p.y
	    });

	    this.prevSheet = 'marioR';

	    this.on('enemyHit', function() {this.p.dead = true; 
	    								this.del("platformerControls");
	    								this.p.gravity = 0;
	    								this.p.vx = 0;
	    								this.p.sheet = 'marioDie'; 
	    								this.play('die');});
	    this.on('destroy', function() {
	    			this.destroy();
                    Q.stageScene("endGame", 1, { label: "YOU DIED" }); 
        });

	    this.add('2d, platformerControls, animation');	

	  },

	  step: function(dt) {
	  	console.log(this.p.x);

	  	if (this.p.y > 600) {
	  		this.trigger('enemyHit');
	  	} 

	  	if (!this.p.dead) {
		  	if (this.p.vy != 0) {	  		
		  		this.p.sheet = 'marioJump';
		  		this.play('jump');
			} else if (this.p.vx != 0) {
			  	if (this.p.vx > 0) {
			  		this.p.sheet = 'marioR';
			  	} else {
			  		this.p.sheet = 'marioL';
			  	} 
			  	this.prevSheet = this.p.sheet;
			  	this.play('run');
		  	} else {
		  		this.p.sheet = this.prevSheet;
		  	 	this.play('idle');	
		  	}
	  	}
	  }

	});

	Q.Sprite.extend( "Goomba", {
		init: function(p) {
			this._super({
				sheet: "goomba",
				sprite: 'enemy',
                vx: 100,
                x: p.x,
                y: p.y
			});

			this.idleSheet = 'goomba';
			this.dieSheet = 'goombaDie';

            this.add('2d, aiBounce, defaultEnemy, animation, startingPosition');   

            this.play('idle');        
        }
	});

	Q.Sprite.extend( "Bloopa", {

		init: function(p) {
			this._super({
				sheet: "bloopa",
				sprite: 'enemy',
				vy: 70,				
                x: p.x,
                y: p.y
			});

			this.idleSheet = 'bloopa';
			this.dieSheet = 'bloopaDie';
			this.startY = this.p.y;

            this.add('2d, defaultEnemy, animation');  

            this.play('idle'); 

            this.p.gravity = 0;

            this.maxDistance = 100;     
        },

        step: function(dt) {
			var delta = this.startY - this.p.y;

			if (delta <= 0 || delta >= this.maxDistance) {
				this.p.vy *= -1;
			}
		}

	});

	Q.Sprite.extend( "Princess", {
		init: function(p) {
			this._super({				
				asset: "princess.png",
				sensor: true,				
                x: p.x,
                y: p.y
			});

			this.add('2d');		

			this.on("sensor", function(other) {
				if (other.isA("Mario")) {
                    Q.stageScene("endGame", 1, { label: "VICTORY ACHIEVED" }); 					
				}
			});
		}
	});

	Q.Sprite.extend("Coin", {
		init: function(p) {
			this._super({				
				sheet: "coin",
				sprite: 'coin',
				sensor: true,
                x: p.x,
                y: p.y
			});

			this.add('2d, animation, tween');

			this.p.gravity = 0;
			this.play('idle');

			this.on('sensor', function(other) {
				if (other.isA("Mario")) {
					this.play('collect');
					this.animate({y: this.p.y - 15}, 
								 {callback: function(){
									Q.state.inc("score",1);	
									this.destroy();}
								});									
				}
			});			
		}

	});

	/* === Load Assets === */	

	Q.loadTMX("level.tmx, tiles.png, bg.png, mario_small.png, mario_small.json, coin.png, \
		coin.json, goomba.png, goomba.json, bloopa.png, bloopa.json, \
		princess.png, piranha.png, mainTitle.png", function() {
		Q.compileSheets("mario_small.png", "mario_small.json");
		Q.compileSheets("coin.png", "coin.json");
		Q.compileSheets("goomba.png", "goomba.json");
		Q.compileSheets("bloopa.png", "bloopa.json");
		  
		Q.stageScene("intro");
	});

	/* === Scenes === */

	var locations = {
		mario: [{x: 150, y: 528}],
		goombas: [{x: 1000, y: 520}, {x: 1500, y: 520}, {x: 500, y: 520}],
		bloopas: [{x: 1500, y: 450}],
		princesses: [{x: 2000, y: 300}],
		coins: [{x: 200, y: 300}, {x: 100, y: 300}, {x: 1000, y: 300}, {x: 300, y: 300}]
	};

	/* === Scenes === */

	Q.scene("level",function(stage) {
		Q.stageTMX("level.tmx", stage);
	    Q.state.reset({ score: 0});

	    // Mario
		var mario = stage.insert(new Q.Mario(locations.mario[0]));
		// Goombas
		locations.goombas.map(function(coord) {
			var goomba = stage.insert(new Q.Goomba(coord));
		});

		// Bloopas
		locations.bloopas.map(function(coord) {
			var bloopa = stage.insert(new Q.Bloopa(coord));
		});

		// Princesses
		locations.princesses.map(function(coord) {
			var princess = stage.insert(new Q.Princess(coord));
		});

		// Coins
		locations.bloopas.map(function(coord) {
			var coin = stage.insert(new Q.Coin(coord));
		});

	    stage.add("viewport").follow(mario, { x: true, y: false });
	    stage.viewport.centerOn(0, 320);
	    stage.viewport.offset(-50, 200);

	});
	
	Q.scene('intro', function(stage) {		

		var button = stage.insert(new Q.UI.Button({ x: Q.width / 2, y: Q.height / 2, asset: "mainTitle.png" }));    		

		button.on("click",function() {
			Q.clearStages();
			Q.stageScene('level');
			Q.stageScene('hud', 1);
		});	  

	});

	Q.scene('endGame',function(stage) {
		var container = stage.insert(new Q.UI.Container({
			x: Q.width/2, y: Q.height/2, fill: "rgba(0,0,0,1)"
		}));
		
		var button = container.insert(new Q.UI.Button({ x: 0, y: 0, fill: "#F0F0F0", color: "white", family: "Papyrus",
		                                                label: "Play Again"}))         
		var label = container.insert(new Q.UI.Text({x: 0, y: -20 - button.p.h, family: "Papyrus", color: "red", 
		                                                 label: stage.options.label }));	  
		button.on("click",function() {
			Q.clearStages();
			Q.stageScene('intro');
		});	  
		
		container.fit(20, Q.width);
	});

	Q.scene('hud',function(stage) {
	  var container = stage.insert(new Q.UI.Container({
	    x: 50, y: 0
	  }));

	  var label = container.insert(new Q.UI.Text({x: 30, y: 10,
	    label: "SOULS " + Q.state.get("score"), color: "red", border: 10, family: "Papyrus" }));	  

	  container.fit(20);

	  Q.state.on("change.score", function() {label.p.label =  "SOULS " + Q.state.get('score');});

	});

});
