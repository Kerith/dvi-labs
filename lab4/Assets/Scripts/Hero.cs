﻿using UnityEngine;
using System.Collections;
using KWorks.Wrappers; // for Transform.SetX, Transform.SetY

public class Hero : MonoBehaviour {

	static public Hero S;		// Singleton
	public float speed = 30;
	public float rollMult = -45;
	public float pitchMult = 30;
	public float gameRestartDelay = 2f;

	[SerializeField]
	private	float _shieldLevel = 1;

	public Weapon[] weapons;

	public float shieldLevel {
		get {
			return (_shieldLevel);
		}
		set {
			_shieldLevel = Mathf.Min(value, 4);
			if (value < 0) {
				Destroy(this.gameObject);
				Main.S.DelayedRestart(gameRestartDelay);
			}
		}
	}

	private Transform tr;
	public Bounds bounds;

	public GameObject lastTriggerGo = null;

	public delegate void WeaponFireDelegate();
	public WeaponFireDelegate fireDelegate;

	void Awake() {
		S =	this;	// Set the Singleton
		bounds = Utils.CombineBoundsOfChildren(this.gameObject);
	}

	void Start() {
		tr = GetComponent<Transform> ();

		ClearWeapons();
		weapons[0].SetType(WeaponType.blaster);
	}

	void Update () {
		// Pull in information from the Input class
		float xAxis = Input.GetAxis("Horizontal");
		float yAxis = Input.GetAxis("Vertical");

		bounds.center = tr.position;
		Vector3 off = Utils.ScreenBoundsCheck(bounds, BoundsTest.onScreen);
		
		// Use wrappers to ease the position changes
		tr.SetX(tr.position.x + (xAxis * speed * Time.deltaTime) - off.x);
		tr.SetY(tr.position.y + (yAxis * speed * Time.deltaTime) - off.y);

		tr.rotation = Quaternion.Euler (yAxis*pitchMult, xAxis*rollMult, 0);

		if (Input.GetAxis("Jump") == 1 && fireDelegate != null) {
			fireDelegate();
		}
	}

	void OnTriggerEnter2D(Collider2D other) {
		Debug.Log("Triggered: " + other.gameObject.name);

		GameObject go = Utils.FindTaggedParent(other.gameObject);
		if (go != null) {
			if (go == lastTriggerGo) {
				return;
			}
			lastTriggerGo = go;
			
			if (go.tag == "Enemy") {
				shieldLevel--;
				Destroy(go);
			} else if (go.tag == "PowerUp") {
				AbsorbPowerUp(go);
			} else {
				print("Triggered: " + go.name);
			}
		} else {
			print("Triggered: " + other.gameObject.name);
		}
	}

	public void AbsorbPowerUp(GameObject go) {
		PowerUp pu = go.GetComponent<PowerUp>();
		switch(pu.type) {
			case WeaponType.shield:
				shieldLevel++;
				break;

			default:
				if (pu.type == weapons[0].type) {
					Weapon w = GetEmptyWeaponSlot();
					if (w != null) {
						w.SetType(pu.type);
					}
				} else {
					ClearWeapons();
					weapons[0].SetType(pu.type);
				}
				break;
		}
		pu.AbsorbedBy(this.gameObject);
	}

	Weapon GetEmptyWeaponSlot() {
		for (int i = 0; i < weapons.Length; i++) {
			if (weapons[i].type == WeaponType.none) {
				return weapons[i];
			}
		}
		return null;
	}

	void ClearWeapons() {
		foreach (Weapon w in weapons) {
			w.SetType(WeaponType.none);
		}
	}
}
