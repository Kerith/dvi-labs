﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using KWorks.Wrappers;

public class Main : MonoBehaviour {

	static public Main S;
	static public Dictionary<WeaponType, WeaponDefinition> W_DEFS;

	public GameObject[] prefabEnemies;
	public float enemySpawnPerSecond = 0.5f;
	public float enemySpawnPadding = 1.5f;
	private float enemySpawnRate;

	public WeaponDefinition[] weaponDefinitions;
	public WeaponType[] activeWeaponTypes;

	public GameObject prefabPowerUp;
	public WeaponType[] powerUpFrequency = new WeaponType[] {
											WeaponType.blaster,
											WeaponType.blaster,
											WeaponType.spread,
											WeaponType.shield
											};

	private int totalScore;

	// Use this for initialization
	void Awake () {
		S = this;

		Utils.SetCameraBounds(this.GetComponent<Camera>());
		enemySpawnRate = 1f/enemySpawnPerSecond;
		Invoke("SpawnEnemy", enemySpawnRate);

		W_DEFS = new Dictionary<WeaponType, WeaponDefinition>();
		foreach (WeaponDefinition def in weaponDefinitions) {
			W_DEFS[def.type] = def;
		}
	}

	static public WeaponDefinition GetWeaponDefinition (WeaponType wt) {
		if (W_DEFS.ContainsKey(wt)) {
			return (W_DEFS[wt]);
		}
        Debug.Log("No weapon definition found");
		return (new WeaponDefinition());		
	}

	void Start() {
		activeWeaponTypes = new WeaponType[weaponDefinitions.Length];
		for (int i = 0; i<weaponDefinitions.Length; i++) {
			activeWeaponTypes[i] = weaponDefinitions[i].type;
		}
		totalScore = 0;
		GUIManager.S.UpdateScore(totalScore);
        
	}
	
	public void SpawnEnemy() {
		int ndx = Random.Range(0, prefabEnemies.Length);
		GameObject go = Instantiate(prefabEnemies[ndx]) as GameObject;
		float xMin = Utils.camBounds.min.x + enemySpawnPadding;
		float xMax = Utils.camBounds.max.x + enemySpawnPadding;
		go.transform.SetX(Random.Range(xMin, xMax));
		go.transform.SetY(Utils.camBounds.max.y + enemySpawnPadding);

		Invoke ("SpawnEnemy", enemySpawnRate);
	}

	public void DelayedRestart(float delay) {
		Invoke("Restart", delay);
	}

	public void Restart() {
		SceneManager.LoadScene("Menu");
	}

	public void ShipDestroyed(Enemy e) {
		if (Random.value <= e.powerUpDropChance) {
			int ndx = Random.Range(0, powerUpFrequency.Length);
			WeaponType puType = powerUpFrequency[ndx];

			GameObject go = Instantiate( prefabPowerUp ) as GameObject;
			PowerUp pu = go.GetComponent<PowerUp>();
			pu.SetType(puType);
			pu.transform.position = e.transform.position;
		}
		addScore(e.score);		
	}

	public void addScore(int inc) {
		totalScore += inc;
		GUIManager.S.UpdateScore(totalScore);
	}

}
