﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum BoundsTest {
	center,
	onScreen,
	offScreen
}

public class Utils : MonoBehaviour {

	public static Bounds BoundsUnion(Bounds b0, Bounds b1) {
		if (b0.size == Vector3.zero && b1.size != Vector3.zero) {
			return(b1);
		} else if (b0.size != Vector3.zero && b1.size == Vector3.zero) {
			return (b0);
		} else if (b0.size == Vector3.zero && b1.size == Vector3.zero) {
			return (b0);
		}

		b0.Encapsulate(b1.min);
		b0.Encapsulate(b1.max);
		return (b0);
	}

	public static Bounds CombineBoundsOfChildren(GameObject go) {
		// Create an empty Bounds b
		Bounds b = new Bounds(Vector3.zero, Vector3.zero);
		Renderer renderer = go.GetComponent<Renderer>();
		Collider collider = go.GetComponent<Collider>();
		// If this GameObject has a Renderer Component...
		if (renderer != null ) {// Expand b to contain the Renderer's Bounds
			b = BoundsUnion(b, renderer.bounds);
		}
		// If this GameObject has a Collider Component...
		if(collider != null) {// Expand b to contain the Collider's Bounds
			b = BoundsUnion(b, collider.bounds);
		}
		// Recursively iterate through each child of this gameObject.transform
		foreach( Transform t in go.transform ) { // Expand b to contain their Bounds as well
			b = BoundsUnion( b, CombineBoundsOfChildren( t.gameObject ) );
		}
		return( b );
	}

	// Make a static read-only public property camBounds
	static public Bounds camBounds {// 1
		get	{
			// if _camBounds hasn't been set yet
			if(_camBounds.size == Vector3.zero) {
				// SetCameraBounds using the default Camera
				SetCameraBounds();
			}
			return( _camBounds );
		}
	}
	// This is the private static field that camBounds uses
	static private Bounds _camBounds;
	// This function is used by camBounds to set _camBounds and can also be
	//  called directly.
	public static void SetCameraBounds(Camera cam=null) {
		// If no Camera was passed in, use the main Camera
		if(cam ==null) cam = Camera.main;
		// This makes a couple of important assumptions about the camera!:
		//   1. The camera is Orthographic
		//   2. The camera is at a rotation of R:[0,0,0]
		// Make Vector3s at the topLeft and bottomRight of the Screen coords
		Vector3 topLeft = new Vector3( 0, 0, 0 );
		Vector3 bottomRight = new Vector3( Screen.width, Screen.height, 0 );
		// Convert these to world coordinates
		Vector3 boundTLN = cam.ScreenToWorldPoint( topLeft );
		Vector3 boundBRF = cam.ScreenToWorldPoint( bottomRight );
		// Adjust their zs to be at the near and far Camera clipping planes
		boundTLN.z += cam.nearClipPlane;
		boundBRF.z += cam.farClipPlane;
		// Find the center of the Bounds
		Vector3 center = (boundTLN + boundBRF)/2f;
		_camBounds = new Bounds( center, Vector3.zero );
		// Expand _camBounds to encapsulate the extents.
		_camBounds.Encapsulate( boundTLN );
		_camBounds.Encapsulate( boundBRF );
	}

	public static Vector3 ScreenBoundsCheck(Bounds bnd, 
											BoundsTest test = BoundsTest.center) {
		return ( BoundsInBoundsCheck( camBounds, bnd, test ) );
	}

	public static Vector3 BoundsInBoundsCheck(Bounds bigB, Bounds lilB,
												BoundsTest test = BoundsTest.onScreen) {
		Vector3 pos = lilB.center;

		Vector3 off = Vector3.zero;

		switch(test) {
			case BoundsTest.center:
				if (bigB.Contains(pos)) {
					return(Vector3.zero);
				}

				if (pos.x > bigB.max.x) {
					off.x = pos.x - bigB.max.x;
				} else if (pos.x < bigB.min.x) {
					off.x = pos.x - bigB.min.x;
				} 

				if (pos.y > bigB.max.y) {
					off.y = pos.y - bigB.max.y;
				} else if (pos.y < bigB.min.y) {
					off.y = pos.y - bigB.min.y;
				} 

				if (pos.z > bigB.max.z) {
					off.z= pos.z - bigB.max.z;
				} else if (pos.z < bigB.min.z) {
					off.z = pos.z - bigB.min.z;
				} 
				return (off);

			case BoundsTest.onScreen:
				if (bigB.Contains(lilB.min) && bigB.Contains(lilB.max)) {
					return(Vector3.zero);
				}

				if (lilB.max.x > bigB.max.x) {
					off.x = lilB.max.x - bigB.max.x;
				} else if (lilB.min.x < bigB.min.x) {
					off.x = lilB.min.x - bigB.min.x;
				} 
				
				if (lilB.max.y > bigB.max.y) {
					off.y = lilB.max.y - bigB.max.y;
				} else if (lilB.min.y < bigB.min.y) {
					off.y = lilB.min.y - bigB.min.y;
				}

				if (lilB.max.z > bigB.max.z) {
					off.z = lilB.max.z - bigB.max.z;
				} else if (lilB.min.z < bigB.min.z) {
					off.z = lilB.min.z - bigB.min.z;
				} 

				return (off);

			case BoundsTest.offScreen:
				bool cMin = bigB.Contains( lilB.min );
				bool cMax = bigB.Contains( lilB.max );
				if (cMin || cMax) {
					return(Vector3.zero);
				}

				if (lilB.min.x > bigB.max.x) {
					off.x = lilB.min.x - bigB.max.x;
				} else if (lilB.max.x < bigB.min.x) {
					off.x = lilB.max.x - bigB.min.x;
				} 
				
				if (lilB.min.y > bigB.max.y) {
					off.y = lilB.max.y - bigB.max.y;
				} else if (lilB.max.y < bigB.min.y) {
					off.y = lilB.max.y - bigB.min.y;
				}

				if (lilB.min.z > bigB.max.z) {
					off.z = lilB.min.z - bigB.max.z;
				} else if (lilB.max.z < bigB.min.z) {
					off.z = lilB.max.z - bigB.min.z;
				} 

				return (off);
		}

		return (Vector3.zero);
	}

	public static GameObject FindTaggedParent(GameObject go) {
		if (go.tag != "Untagged") {
			return (go);
		}

		if (go.transform.parent == null) {
			return (null);			
		}

		return (FindTaggedParent(go.transform.parent.gameObject));
	}

	public static GameObject FindTaggedParent(Transform t) {
		return (FindTaggedParent(t.gameObject));
	}

    public static Material[] GetAllMaterials(GameObject go)
    {
        List<Material> mats = new List<Material>();
        Renderer r = go.GetComponent<Renderer>();
        if (r != null) {
            mats.Add(r.material);
        }
        foreach (Transform t in go.GetComponent<Transform>()) {
            mats.AddRange(GetAllMaterials(t.gameObject));
        }
        return (mats.ToArray());
    }
}
