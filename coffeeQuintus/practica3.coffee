window.addEventListener("load", ->

	Q = Quintus()
		.include("Sprites, Scenes, Input, 2D, Touch, UI, TMX, Anim")
		.setup({width: 320, height: 480})
		.controls().touch();

	# === Load Assets === #	

	Q.loadTMX("level.tmx, tiles.png, bg.png, mario_small.png, mario_small.json, coin.png, \
		coin.json, goomba.png, goomba.json, bloopa.png, bloopa.json, \
		princess.png, piranha.png, mainTitle.png", ->
			Q.compileSheets("mario_small.png", "mario_small.json");
			Q.compileSheets("coin.png", "coin.json");
			Q.compileSheets("goomba.png", "goomba.json");
			Q.compileSheets("bloopa.png", "bloopa.json");
			  
			Q.stageScene("intro");
	);

)
